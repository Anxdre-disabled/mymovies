package id.andre.mymovies.view.movielistfragment

import id.andre.mymovies.data.model.Movie

interface MovieListView {
    fun showDetailMovies(id: String, type: String)
    fun attachData(mMovies: List<Movie.Result>)
    fun showNetworkError()
    fun hideLoading()
    fun showLoading()
}