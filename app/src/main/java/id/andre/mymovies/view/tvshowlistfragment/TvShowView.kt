package id.andre.mymovies.view.tvshowlistfragment

import id.andre.mymovies.data.model.TvShow

interface TvShowView {
    fun showDetailMovies(id: String, type: String)
    fun attachData(mTvShow: List<TvShow.Result>)
    fun showNetworkError()
    fun hideLoading()
    fun showLoading()
}