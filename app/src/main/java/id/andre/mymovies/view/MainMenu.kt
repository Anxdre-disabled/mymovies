package id.andre.mymovies.view

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import id.andre.mymovies.R
import id.andre.mymovies.adapter.MainMenuAdapter
import kotlinx.android.synthetic.main.activity_main_menu.*

class MainMenu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        vp_main_menu.adapter = MainMenuAdapter(this,supportFragmentManager)
        tb_main_menu.setupWithViewPager(vp_main_menu)

        btn_language.setOnClickListener { startActivity(Intent(Settings.ACTION_LOCALE_SETTINGS)) }
    }
}
