package id.andre.mymovies.view.itemdetail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import id.andre.mymovies.R
import id.andre.mymovies.data.api.ApiFactory
import id.andre.mymovies.data.model.OfflineDetailItem
import id.andre.mymovies.util.getImage
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ItemDetailActivity : AppCompatActivity(), ItemDetailView {
    private val mApi by lazy { ApiFactory() }
    private val mPresenter by lazy { ItemDetailPresenter(this, mApi) }

    private val titleKey = "TITLE"
    private val backdropKey = "BACKDROP"
    private val posterKey = "POSTER"
    private val descKey = "DESC"

    private var mTitle: String? = null
    private var mBackdrop: String? = null
    private var mPoster: String? = null
    private var mDesc: String? = null

    private var mFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        val mId = intent.getStringExtra("EXTRA")
        val mCategory = intent.getStringExtra("CATEGORY")

        if (savedInstanceState != null) {
            mTitle = savedInstanceState.getString(titleKey)
            mBackdrop = savedInstanceState.getString(backdropKey)
            mPoster = savedInstanceState.getString(posterKey)
            mDesc = savedInstanceState.getString(descKey)

            showData(mTitle as String, mBackdrop as String, mDesc as String, mPoster as String)
        } else {
            if (mId != null) {
                if (mCategory == "MOVIE") {
                    GlobalScope.launch(Dispatchers.Main) { mPresenter.reqMovieData(mId) }
                } else if (mCategory == "TV_SHOW") {
                    GlobalScope.launch(Dispatchers.Main) { mPresenter.reqTvData(mId) }
                }
            }
        }

        sr_movie_detail.setOnRefreshListener {
            GlobalScope.launch(Dispatchers.Main) {
                mPresenter.reqMovieData(
                    mId
                )
            }
        }

        fab_favorite.setOnClickListener {
            if (!mFavorite) {
                mPresenter.saveItemToDatabase(
                    mId.toInt(),
                    mTitle,
                    mCategory,
                    mBackdrop,
                    mPoster,
                    mDesc
                )
                showDeleteButton()
            } else {
                mPresenter.deleteFromDatabase(
                    OfflineDetailItem(
                        mId.toInt(),
                        mCategory,
                        mTitle!!,
                        mBackdrop!!,
                        mPoster!!,
                        mDesc!!
                    )
                )
                showFavoriteButton()
                finish()
            }
        }
    }

    override fun showData(title: String, imageUrl: String, desc: String, poster: String) {
        tv_movie_title.text = title
        getImage(imageUrl, iv_movie_poster)
        tv_movie_description.text = desc

        mTitle = title
        mBackdrop = imageUrl
        mPoster = poster
        mDesc = desc
    }

    override fun showNetworkError() {
        MaterialDialog(applicationContext).show {
            title(R.string.network_error_tittle)
            message(R.string.network_error_desc)
        }
    }

    override fun hideLoading() {
        sr_movie_detail.isRefreshing = false
    }

    override fun showLoading() {
        sr_movie_detail.isRefreshing = true
    }

    override fun showFavoriteButton() {
        mFavorite = false
        fab_favorite.setImageResource(R.drawable.ic_add_black_24dp)
    }

    override fun showDeleteButton() {
        mFavorite = true
        fab_favorite.setImageResource(R.drawable.ic_delete_forever_black_24dp)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(titleKey, mTitle)
        outState.putString(backdropKey, mBackdrop)
        outState.putString(descKey, mDesc)
        super.onSaveInstanceState(outState)
    }
}
