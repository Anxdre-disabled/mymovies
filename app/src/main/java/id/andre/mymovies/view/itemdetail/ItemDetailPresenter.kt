package id.andre.mymovies.view.itemdetail

import id.andre.mymovies.data.api.ApiRepository
import id.andre.mymovies.data.database.DbModule
import id.andre.mymovies.data.model.OfflineDetailItem

class ItemDetailPresenter(private val mView: ItemDetailView, private val mApi: ApiRepository) {

    suspend fun reqMovieData(id: String) {
        mView.showLoading()
        val offlineData = loadItemFromDatabase(id)

        if (offlineData != null) {
            mView.showDeleteButton()
            mView.showData(
                offlineData.title,
                offlineData.imagePath,
                offlineData.descItem,
                offlineData.poster
            )
            mView.hideLoading()
        } else {
            mView.showFavoriteButton()
            val data = mApi.getMoviesDetailAsync(id).await()

            if (data != null) {
                mView.showData(
                    data.originalTitle,
                    data.backdropPath,
                    data.overview,
                    data.posterPath
                )
                mView.hideLoading()
            } else {
                mView.showNetworkError()
                mView.hideLoading()
            }

        }
    }

    suspend fun reqTvData(id: String) {
        mView.showLoading()
        val offlineData = loadItemFromDatabase(id)

        if (offlineData == null) {
            mView.showFavoriteButton()
            val data = mApi.getTvShowDetailAsync(id).await()

            if (data != null) {
                mView.showData(
                    data.name,
                    data.backdropPath,
                    data.overview,
                    data.posterPath
                )
                mView.hideLoading()
            } else {
                mView.showNetworkError()
                mView.hideLoading()
            }

        } else {
            mView.showDeleteButton()
            mView.showData(
                offlineData.title,
                offlineData.imagePath,
                offlineData.descItem,
                offlineData.poster
            )
            mView.hideLoading()
        }
    }

    fun saveItemToDatabase(
        id: Int?,
        title: String?,
        category: String?,
        imagePath: String?,
        poster: String?,
        descItem: String?
    ) {
        if (id != null) {
            val offlineData =
                OfflineDetailItem(id, category!!, title!!, imagePath!!, poster!!, descItem!!)
            DbModule.RoomDatabase?.offlineDao?.insert(offlineData)
        }
    }

    fun deleteFromDatabase(data: OfflineDetailItem) {
        DbModule.RoomDatabase?.offlineDao?.deleteItem(data)
    }

    private fun loadItemFromDatabase(id: String): OfflineDetailItem? {
        return DbModule.RoomDatabase?.offlineDao?.selectOnRow(id.toInt())
    }
}