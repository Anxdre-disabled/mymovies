package id.andre.mymovies.view.tvshowlistfragment

import id.andre.mymovies.data.api.ApiRepository

class TvShowPresenter(private val mView: TvShowView, private val mApi: ApiRepository) {

    suspend fun initData() {
        mView.showLoading()
        val data = mApi.getTvShowAsync().await()?.results
        if (data != null && data.isNotEmpty()) {
            mView.attachData(data)
            mView.hideLoading()
        } else {
            mView.showNetworkError()
            mView.hideLoading()
        }
    }
}