package id.andre.mymovies.view.movielistfragment

import id.andre.mymovies.data.api.ApiRepository

class MovieListPresenter(
    private val mView: MovieListView,
    private val mApi: ApiRepository
) {
    suspend fun initData() {
        mView.showLoading()
        val data = mApi.getPopularMoviesAsync().await()?.results
        if (data != null && data.isNotEmpty()) {
            mView.attachData(data)
            mView.hideLoading()
        } else {
            mView.showNetworkError()
            mView.hideLoading()
        }
    }
}