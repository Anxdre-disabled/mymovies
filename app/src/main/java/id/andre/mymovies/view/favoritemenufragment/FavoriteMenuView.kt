package id.andre.mymovies.view.favoritemenufragment

import id.andre.mymovies.data.model.OfflineDetailItem

interface FavoriteMenuView {
    fun showDetail(id: String, type: String)
    fun attachDataMovie(mMovies: List<OfflineDetailItem>)
    fun attachDataTvShow(mMovies: List<OfflineDetailItem>)
    fun showNetworkError()
    fun showNoData(category: String)
    fun showNothingSelected()
    fun hideLoading()
    fun showLoading()
}