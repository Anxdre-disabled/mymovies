package id.andre.mymovies.view.favoritemenufragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import id.andre.mymovies.R
import id.andre.mymovies.adapter.FavoriteListAdapter
import id.andre.mymovies.data.model.OfflineDetailItem
import id.andre.mymovies.view.itemdetail.ItemDetailActivity
import kotlinx.android.synthetic.main.fragment_menu.*

class FavoriteMenu : Fragment(), FavoriteMenuView {

    private val mPresenter by lazy { FavoriteMenuPresenter(this) }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_dropdown_item_1line,
            resources.getStringArray(R.array.category)
        )
        spinner_menu.adapter = adapter
        spinner_menu.visibility = View.VISIBLE
        super.onViewCreated(view, savedInstanceState)

        sr_menu.setOnRefreshListener { mPresenter.initData(spinner_menu.selectedItemPosition) }

        spinner_menu.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                showNothingSelected()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                mPresenter.initData(spinner_menu.selectedItemPosition)
            }
        }
    }

    override fun showDetail(id: String, type: String) {
        val detailIntent = Intent(context, ItemDetailActivity::class.java)
        detailIntent.putExtra("EXTRA", id)
        detailIntent.putExtra("CATEGORY", type)
        startActivity(detailIntent)
    }

    override fun attachDataMovie(mMovies: List<OfflineDetailItem>) {
        with(rv_list_movie) {
            layoutManager = LinearLayoutManager(context)
            adapter = (FavoriteListAdapter(mMovies) {
                showDetail(it.id.toString(), "MOVIE")
            })
        }
    }

    override fun attachDataTvShow(mMovies: List<OfflineDetailItem>) {
        with(rv_list_movie) {
            layoutManager = LinearLayoutManager(context)
            adapter = (FavoriteListAdapter(mMovies) {
                showDetail(it.id.toString(), "TV_SHOW")
            })
        }
    }

    override fun showNetworkError() {
        MaterialDialog(requireContext()).show {
            title(R.string.network_error_tittle)
            message(R.string.network_error_desc)
        }
    }

    override fun hideLoading() {
        sr_menu.isRefreshing = false
    }

    override fun showLoading() {
        sr_menu.isRefreshing = true
    }

    override fun showNoData(category: String) {
        if (category == "MOVIE") {
            Toast.makeText(
                requireContext(),
                context?.getText(R.string.no_data_movie),
                Toast.LENGTH_SHORT
            ).show()
        } else {
            Toast.makeText(
                requireContext(),
                context?.getText(R.string.no_data_tv),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun showNothingSelected() {
        Toast.makeText(requireContext(), "Nothing Selected", Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        rv_list_movie.adapter = null
        mPresenter.initData(spinner_menu.selectedItemPosition)
        super.onResume()
    }

}