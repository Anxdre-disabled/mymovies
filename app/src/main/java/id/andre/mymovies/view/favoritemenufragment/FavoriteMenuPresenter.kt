package id.andre.mymovies.view.favoritemenufragment

import id.andre.mymovies.data.database.DbModule

class FavoriteMenuPresenter(private val mView: FavoriteMenuView) {
    fun initData(id: Int) {
        if (id == 0) {
            getFavoriteMovie()
        } else if (id == 1) {
            getFavoriteTvShow()
        }
    }

    private fun getFavoriteMovie() {
        val data = DbModule.RoomDatabase?.offlineDao?.getAllItem("MOVIE")
        if (data.isNullOrEmpty()) {
            mView.showNoData("MOVIE")
        } else mView.attachDataMovie(data)
        mView.hideLoading()
    }

    private fun getFavoriteTvShow() {
        val data = DbModule.RoomDatabase?.offlineDao?.getAllItem("TV_SHOW")
        if (data.isNullOrEmpty()) {
            mView.showNoData("TV_SHOW")
        } else mView.attachDataTvShow(data)
        mView.hideLoading()
    }

}