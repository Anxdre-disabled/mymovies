package id.andre.mymovies.view.itemdetail

interface ItemDetailView {
    fun showData(title: String, imageUrl: String, desc: String, poster: String)
    fun showNetworkError()
    fun hideLoading()
    fun showLoading()
    fun showFavoriteButton()
    fun showDeleteButton()
}