package id.andre.mymovies.view.movielistfragment

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import id.andre.mymovies.R
import id.andre.mymovies.adapter.MovieAdapter
import id.andre.mymovies.data.api.ApiFactory
import id.andre.mymovies.data.model.Movie
import id.andre.mymovies.view.itemdetail.ItemDetailActivity
import kotlinx.android.synthetic.main.fragment_menu.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MovieList : Fragment(), MovieListView {
    private val apiFactory by lazy { ApiFactory() }
    private val mPresenter by lazy { MovieListPresenter(this, apiFactory) }

    private var listState: Parcelable? = null
    private val stateKey = "recycler_list_state"
    private val dataStateKey = "data_state"

    private var dataState: ArrayList<Movie.Result> = ArrayList()
    private var data: List<Movie.Result>? = mutableListOf()

    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mLayoutManager = LinearLayoutManager(context)
        if (savedInstanceState != null) {
            listState = savedInstanceState.getParcelable(stateKey)
            dataState = savedInstanceState.getParcelableArrayList(dataStateKey)

            data = dataState
            data?.let { attachData(it) }
        } else {
            GlobalScope.launch(Dispatchers.Main) { mPresenter.initData() }
        }
        sr_menu.setOnRefreshListener { GlobalScope.launch(Dispatchers.Main) { mPresenter.initData() } }
    }

    override fun attachData(mMovies: List<Movie.Result>) {
        with(rv_list_movie) {
            layoutManager = mLayoutManager
            adapter = (MovieAdapter(mMovies) {
                showDetailMovies(it.id.toString(), "MOVIE")
            })
        }
        dataState.addAll(mMovies)
    }

    override fun showDetailMovies(id: String, type: String) {
        val detailIntent = Intent(context, ItemDetailActivity::class.java)
        detailIntent.putExtra("EXTRA", id)
        detailIntent.putExtra("CATEGORY", type)
        startActivity(detailIntent)
    }

    override fun showNetworkError() {
        context?.let {
            MaterialDialog(it).show {
                title(R.string.network_error_tittle)
                message(R.string.network_error_desc)
            }
        }
    }

    override fun hideLoading() {
        sr_menu.isRefreshing = false
    }

    override fun showLoading() {
        sr_menu.isRefreshing = true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        listState = rv_list_movie?.layoutManager?.onSaveInstanceState()
        outState.putParcelable(stateKey, listState)
        outState.putParcelableArrayList(dataStateKey, dataState)
        super.onSaveInstanceState(outState)
    }
}