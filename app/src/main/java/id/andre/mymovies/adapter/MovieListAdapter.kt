package id.andre.mymovies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.andre.mymovies.R
import id.andre.mymovies.data.model.Movie
import id.andre.mymovies.util.getImage
import kotlinx.android.synthetic.main.custom_listview_layout.view.*

class MovieAdapter(
    private val dataSource: List<Movie.Result>,
    private val listener: (Movie.Result) -> Unit
) : RecyclerView.Adapter<MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int) = MovieViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.custom_listview_layout , parent , false))

    override fun getItemCount(): Int = dataSource.size

    override fun onBindViewHolder(holder: MovieViewHolder , position: Int) {
        holder.bindItem(dataSource[position] , listener)
    }
}

class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItem(items: Movie.Result, listener: (Movie.Result) -> Unit) {
        itemView.tv_title_movies.text = items.title
        getImage(items.posterPath,itemView.iv_movies)
        itemView.setOnClickListener {
            listener(items)
        }
    }
}