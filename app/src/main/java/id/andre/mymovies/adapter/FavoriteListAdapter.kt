package id.andre.mymovies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.andre.mymovies.R
import id.andre.mymovies.data.model.OfflineDetailItem
import id.andre.mymovies.util.getImage
import kotlinx.android.synthetic.main.custom_listview_layout.view.*

class FavoriteListAdapter(
    private val dataSource: List<OfflineDetailItem>,
    private val listener: (OfflineDetailItem) -> Unit
) : RecyclerView.Adapter<FavoriteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = FavoriteViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.custom_listview_layout, parent, false)
    )

    override fun getItemCount(): Int = dataSource.size

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        holder.bindItem(dataSource[position], listener)
    }
}

class FavoriteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItem(items: OfflineDetailItem, listener: (OfflineDetailItem) -> Unit) {
        itemView.tv_title_movies.text = items.title
        getImage(items.poster, itemView.iv_movies)
        itemView.setOnClickListener {
            listener(items)
        }
    }
}