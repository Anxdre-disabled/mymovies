package id.andre.mymovies.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.andre.mymovies.R
import id.andre.mymovies.view.favoritemenufragment.FavoriteMenu
import id.andre.mymovies.view.movielistfragment.MovieList
import id.andre.mymovies.view.tvshowlistfragment.TvShowList

class MainMenuAdapter(private val context: Context, mFragment: FragmentManager) :
    FragmentPagerAdapter(
        mFragment,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                MovieList()
            }
            1 -> {
                TvShowList()
            }
            else -> {
                return FavoriteMenu()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.my_movies)

            1 -> context.getString(R.string.tv_show)

            else -> context.getString(R.string.favorite)
        }
    }
}