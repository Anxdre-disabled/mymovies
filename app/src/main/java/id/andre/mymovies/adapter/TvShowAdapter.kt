package id.andre.mymovies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.andre.mymovies.R
import id.andre.mymovies.data.model.TvShow
import id.andre.mymovies.util.getImage
import kotlinx.android.synthetic.main.custom_listview_layout.view.*

class TvShowAdapter(
    private val dataSource: List<TvShow.Result>,
    private val listener: (TvShow.Result) -> Unit
) : RecyclerView.Adapter<TvShowView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowView = TvShowView(
        LayoutInflater.from(parent.context).inflate(
            R.layout.custom_listview_layout, parent, false
        )
    )

    override fun getItemCount(): Int = dataSource.size

    override fun onBindViewHolder(holder: TvShowView, position: Int) {
        holder.bindItem(dataSource[position], listener)
    }
}

class TvShowView(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItem(items: TvShow.Result, listener: (TvShow.Result) -> Unit) {
        itemView.tv_title_movies.text = items.name
        getImage(items.posterPath, itemView.iv_movies)
        itemView.setOnClickListener {
            listener(items)
        }
    }
}