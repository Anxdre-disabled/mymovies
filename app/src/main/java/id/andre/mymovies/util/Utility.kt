package id.andre.mymovies.util

import android.widget.ImageView
import com.squareup.picasso.Picasso

fun getImage(imageUrl: String?, imageView: ImageView) {
    Picasso.get().load("https://image.tmdb.org/t/p/w500$imageUrl").into(imageView)
}