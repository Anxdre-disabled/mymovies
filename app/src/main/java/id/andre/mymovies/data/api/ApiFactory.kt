package id.andre.mymovies.data.api

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import id.andre.mymovies.BuildConfig
import id.andre.mymovies.data.model.Movie
import id.andre.mymovies.data.model.MovieDetail
import id.andre.mymovies.data.model.TvShow
import id.andre.mymovies.data.model.TvShowDetail
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

@Suppress("UNCHECKED_CAST")
class ApiFactory : ApiRepository {
    override fun getPopularMoviesAsync(): Deferred<Movie?> = reqDataAsync(
        Endpoint.BASE_URL +
                "${Endpoint.POPULAR_MOVIES_URL}${BuildConfig.API_KEY}${Endpoint.EN_LANGUAGE}"
        , Movie::class.java
    ) as Deferred<Movie?>

    override fun getTvShowAsync(): Deferred<TvShow?> = reqDataAsync(
        Endpoint.BASE_URL +
                "${Endpoint.POPULAR_TV_URL}${BuildConfig.API_KEY}${Endpoint.EN_LANGUAGE}", TvShow::class.java
    ) as Deferred<TvShow?>

    override fun getMoviesDetailAsync(id: String): Deferred<MovieDetail?> = reqDataAsync(
        Endpoint.BASE_URL +
                "${Endpoint.DETAIL_MOVIE}$id?${BuildConfig.API_KEY}", MovieDetail::class.java
    ) as Deferred<MovieDetail?>

    override fun getTvShowDetailAsync(id: String): Deferred<TvShowDetail?> = reqDataAsync(
        Endpoint.BASE_URL +
                "${Endpoint.DETAIL_TV}$id?${BuildConfig.API_KEY}", TvShowDetail::class.java
    ) as Deferred<TvShowDetail?>


    private fun reqDataAsync(url: String, type: Class<*>): Deferred<Any> {
        Log.e("url", url)
        return GlobalScope.async {
            val response = AndroidNetworking
                .get(url)
                .setTag("fetchData")
                .setPriority(Priority.MEDIUM)
                .build()
                .executeForObject(type)
            response.result
        }
    }
}