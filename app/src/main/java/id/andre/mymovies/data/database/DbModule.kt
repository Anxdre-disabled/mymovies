package id.andre.mymovies.data.database

import android.app.Application
import androidx.room.Room

class DbModule : Application() {
    companion object {
        var RoomDatabase: RoomDbEngine? = null
    }

    override fun onCreate() {
        super.onCreate()
        RoomDatabase = Room.databaseBuilder(
            applicationContext, RoomDbEngine::class.java,
            "OfflineItem"
        ).allowMainThreadQueries().build()
    }
}