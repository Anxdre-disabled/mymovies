package id.andre.mymovies.data.database

import androidx.room.*
import id.andre.mymovies.data.model.OfflineDetailItem

@Dao
interface ItemOfflineDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(offlineLocation: OfflineDetailItem)

    @Delete
    fun deleteItem(offlineLocation: OfflineDetailItem)

    @Query("SELECT * FROM OfflineDetailItem WHERE category = :category")
    fun getAllItem(category: String): List<OfflineDetailItem>

    @Query("SELECT * FROM OfflineDetailItem WHERE id = :id")
    fun selectOnRow(id: Int): OfflineDetailItem
}