package id.andre.mymovies.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import id.andre.mymovies.data.model.OfflineDetailItem

@Database(entities = [OfflineDetailItem::class], version = 1)
abstract class RoomDbEngine : RoomDatabase() {
    abstract val offlineDao: ItemOfflineDao
}