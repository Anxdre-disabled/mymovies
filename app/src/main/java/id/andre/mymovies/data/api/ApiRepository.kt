package id.andre.mymovies.data.api

import id.andre.mymovies.data.model.Movie
import id.andre.mymovies.data.model.MovieDetail
import id.andre.mymovies.data.model.TvShowDetail
import id.andre.mymovies.data.model.TvShow
import kotlinx.coroutines.Deferred

interface ApiRepository {
    fun getPopularMoviesAsync(): Deferred<Movie?>
    fun getTvShowAsync(): Deferred<TvShow?>
    fun getMoviesDetailAsync(id:String):Deferred<MovieDetail?>
    fun getTvShowDetailAsync(id:String):Deferred<TvShowDetail?>
}