package id.andre.mymovies.data.model
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class TvShow(
    @SerializedName("page")
    val page: Int = 0,
    @SerializedName("total_results")
    val totalResults: Int = 0,
    @SerializedName("total_pages")
    val totalPages: Int = 0,
    @SerializedName("results")
    val results: List<Result> = listOf()
) {
    @Parcelize
    data class Result(
        @SerializedName("original_name")
        val originalName: String = "",
        @SerializedName("genre_ids")
        val genreIds: List<Int> = listOf(),
        @SerializedName("name")
        val name: String = "",
        @SerializedName("popularity")
        val popularity: Double = 0.0,
        @SerializedName("origin_country")
        val originCountry: List<String> = listOf(),
        @SerializedName("vote_count")
        val voteCount: Int = 0,
        @SerializedName("first_air_date")
        val firstAirDate: String = "",
        @SerializedName("backdrop_path")
        val backdropPath: String = "",
        @SerializedName("original_language")
        val originalLanguage: String = "",
        @SerializedName("id")
        val id: Int = 0,
        @SerializedName("vote_average")
        val voteAverage: Double = 0.0,
        @SerializedName("overview")
        val overview: String = "",
        @SerializedName("poster_path")
        val posterPath: String = ""
    ):Parcelable
}