package id.andre.mymovies.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class OfflineDetailItem(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    @ColumnInfo(name = "category")
    val category: String,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "imagePath")
    val imagePath: String,
    @ColumnInfo(name = "poster")
    val poster: String,
    @ColumnInfo(name = "descItem")
    val descItem: String
)