package id.andre.mymovies.data.api

object Endpoint {
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val POPULAR_MOVIES_URL = "movie/popular?"
    const val POPULAR_TV_URL = "tv/popular?"
    const val EN_LANGUAGE = "&language=en-US&page=1"
    const val DETAIL_MOVIE = "movie/"
    const val DETAIL_TV = "tv/"
}